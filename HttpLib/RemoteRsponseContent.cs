﻿
namespace HttpLib
{
    public class RemoteRsponseContent
    {
        /// <summary>
        /// Http状态码
        /// </summary>
        public int StatusCode { get; set; }
        /// <summary>
        /// 正确时的返回内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 出错时的信息
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// 该对象的字符串表示形式
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("StatusCode:{0}\r\nContent:{1}\r\nErrorMessage:{2}", StatusCode, Content, ErrorMessage);
        }

        /// <summary>
        /// 一个空返回结果
        /// </summary>
        public static RemoteRsponseContent Empty{ get;private set;}

        static RemoteRsponseContent()
        {
            Empty = new RemoteRsponseContent {StatusCode = 400, Content = string.Empty, ErrorMessage = "未知错误"};
        }
    }
}
