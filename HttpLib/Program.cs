﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Net;
using System.Threading;

namespace HttpLib
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetCtripFlights();
            //Get12580Flights();
            AppleSerialNumber();
        }

        static void GetCtripFlights()
        {
            var encoding = Encoding.GetEncoding("gb2312");
            var url = "http://flights.ctrip.com/domesticsearch/search/SearchFirstRouteFlights?DCity1=BJS&ACity1=SZX&SearchType=S&DDate1=2015-05-01&rk=0.6817411158699542135529&CK=6694618A07C7EE9D319203A20255AAEA&r=0.5186814504315424600016";
            var refererUrl = "http://flights.ctrip.com/booking/BJS-SZX-day-1.html";
            var userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";

            var headers = new WebHeaderCollection
                              {
                                  {HttpRequestHeader.Referer, refererUrl},
                                  {HttpRequestHeader.UserAgent, userAgent}
                              };

            var content = HttpClient.Get(url, encoding, headers, null);

            Console.WriteLine(content);
        }

        static void Get12580Flights()
        {
            var encoding = Encoding.UTF8;
            var url = "http://12580.10086.cn/flight/searchFlight.do";
            var postData = new NameValueCollection();
            postData.Add("voyageType", "1");
            postData.Add("fromCity", "PEK");
            postData.Add("toCity", "SZX");
            postData.Add("cfromCity", HttpClient.UrlEncode("北京",encoding));
            postData.Add("ctoCity", HttpClient.UrlEncode("深圳",encoding));
            postData.Add("fromDate", "2015-05-01");
            postData.Add("toDate", "");
            postData.Add("voyageTypevalue", "1");
            postData.Add("cabinName", HttpClient.UrlEncode("不限", encoding));

            var refererUrl = "http://12580.10086.cn/flight/searchFlightIndex.do?voyageType=1&cfromCity=%E5%8C%97%E4%BA%AC&fromCity=BJS&ctoCity=%E6%B7%B1%E5%9C%B3&toCity=SZX&fromDate=2015-05-01&toDate=yyyy-mm-dd&voyageTypevalue=1&cabinName=";
            var userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";
            var cookieValue = "CmProvid=bj; WT_FPC=id=27fc976e9be860032d61418351151138:lv=1426144752040:ss=1426144530195; JSESSIONID=6652400F00F1E33D50CAC49BA3880394-n1; WWWID=www_73; Hm_lvt_07bfdad2b5f00d3c924c5e295e93e7f3=1427182181,1427251431; Hm_lpvt_07bfdad2b5f00d3c924c5e295e93e7f3=1427251431; __utmt=1; __utma=268376974.1377419982.1427190989.1427190989.1427251432.2; __utmb=268376974.1.10.1427251432; __utmc=268376974; __utmz=268376974.1427190989.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); cfromcity=%E5%8C%97%E4%BA%AC; fromcity=PEK; ctocity=%E6%B7%B1%E5%9C%B3; tocity=SZX";

            var headers = new WebHeaderCollection
                              {
                                  {HttpRequestHeader.ContentType, HttpClient.PostFormContentType},
                                  {HttpRequestHeader.UserAgent, userAgent}
                              };

            var cc = HttpClient.BuildCookieContainer(url, cookieValue);
            var content = HttpClient.Post(url, postData, encoding, headers, cc, Timeout.Infinite);

            Console.WriteLine(content);
        }

        static void AppleSerialNumber()
        {
            //F2PNMTGJG5QT
            var encoding = Encoding.UTF8;
            var url = "https://selfsolve.apple.com/wcResults.do";
            var postData = new NameValueCollection();
            postData.Add("sn", "F2PNMTGJG5QT");
            postData.Add("cn", "");
            postData.Add("locale", "");
            postData.Add("caller", "");
            postData.Add("num", "644724");

            var refererUrl = "https://selfsolve.apple.com/agreementWarrantyDynamic.do";
            var userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";
            var cookieValue = "CmProvid=bj; WT_FPC=id=27fc976e9be860032d61418351151138:lv=1426144752040:ss=1426144530195; JSESSIONID=6652400F00F1E33D50CAC49BA3880394-n1; WWWID=www_73; Hm_lvt_07bfdad2b5f00d3c924c5e295e93e7f3=1427182181,1427251431; Hm_lpvt_07bfdad2b5f00d3c924c5e295e93e7f3=1427251431; __utmt=1; __utma=268376974.1377419982.1427190989.1427190989.1427251432.2; __utmb=268376974.1.10.1427251432; __utmc=268376974; __utmz=268376974.1427190989.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); cfromcity=%E5%8C%97%E4%BA%AC; fromcity=PEK; ctocity=%E6%B7%B1%E5%9C%B3; tocity=SZX";

            var headers = new WebHeaderCollection
                              {
                                  {HttpRequestHeader.ContentType, HttpClient.PostFormContentType},
                                  {HttpRequestHeader.Referer, refererUrl},
                                  {HttpRequestHeader.UserAgent, userAgent}
                              };

            var cc = HttpClient.BuildCookieContainer(url, cookieValue);
            var content = HttpClient.Post(url, postData, encoding, headers, null, Timeout.Infinite);

            Console.WriteLine(content);
        }
    }
}
