﻿using System;
using System.Collections.Generic;
using MemcachedDemo.Memcached;

namespace MemcachedDemo
{
    class Program
    {
        private static Random _random = new Random();
        static void Main(string[] args)
        {
            var client = MemcachedClient.GetInstance();

            // key的推荐定义方式
            // 项目:模块:业务属性:值
            // 例如：k:product:seriesId:364
            var key = "test:demo:index:";
            //for (var i = 1; i <= 100; i++)
            //{
            //    client.Set(key + i, _random.Next(), TimeSpan.FromMinutes(5));
            //}

            for (var i = 0; i < 1000;i++ )
            {
                var k = key + _random.Next(1, 100);
                Console.WriteLine("key={0}\tvalue={1}", k, client.Get(k));
            }

            PrintStatus(client);
        }

        static void PrintStatus(MemcachedClient client)
        {
            Console.Out.WriteLine("\r\nDisplaying the socketpool status:");
            foreach (KeyValuePair<string, Dictionary<string, string>> host in client.Status())
            {
                Console.Out.WriteLine("Host: " + host.Key);
                foreach (KeyValuePair<string, string> item in host.Value)
                {
                    Console.Out.WriteLine("\t" + item.Key + ": " + item.Value);
                }
                Console.Out.WriteLine();
            }
        }
    }
}
